FROM rustlang/rust:nightly-slim

LABEL maintainer="kevinstiehl@live.de"

RUN apt update && apt install -y cmake g++ clang && apt clean
RUN apt install -y libssl-dev pkg-config && apt clean && \
	RUSTFLAGS="--cfg procmacro2_semver_exempt" cargo install cargo-tarpaulin
